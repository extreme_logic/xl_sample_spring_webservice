package ph.extremelogic.sample.spring.webservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication(scanBasePackages = "ph.extremelogic")
public class SpringSampleWebService extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(SpringSampleWebService.class, args);
	}
}
