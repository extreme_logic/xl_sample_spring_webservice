package ph.extremelogic.sample.spring.webservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BasicMathController {
	   @RequestMapping(value = "/number")
	   public ResponseEntity<Object> getNumber() {
	      return new ResponseEntity<>("526", HttpStatus.OK);
	   }
}
